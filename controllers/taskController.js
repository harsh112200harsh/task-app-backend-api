const Task = require("../models/taskModel");
const logger = require("../utils/logger");

// Retrieve all tasks
const getAllTasks = async (req, res) => {
    try {
        const tasks = await Task.find({});
        res.status(200).json(tasks);
        logger.info("Retrieved all tasks");
    } catch (error) {
        res.status(500).json({ message: error.message });
        logger.error(`Error retrieving all tasks: ${error.message}`);
    }
};

// Create a new task
const createTask = async (req, res) => {
    const task = new Task(req.body);

    try {
        const savedTask = await task.save();
        res.status(201).json(savedTask);
        logger.info("Created a new task");
    } catch (error) {
        res.status(400).json({ message: error.message });
        logger.error(`Error creating a new task: ${error.message}`);
    }
};

// Retrieve a single task by its ID
const getTaskById = async (req, res) => {
    const { id } = req.params;

    try {
        const task = await Task.findById(id);
        if (!task) {
            res.status(404).json({ message: "Task not found" });
            logger.warn(`Task not found: ${id}`);
            return;
        }
        res.status(200).json(task);
        logger.info(`Retrieved task: ${id}`);
    } catch (error) {
        res.status(500).json({ message: error.message });
        logger.error(`Error retrieving task: ${id} - ${error.message}`);
    }
};

// Update an existing task
const updateTask = async (req, res) => {
    const { id } = req.params;
    const updates = req.body;

    try {
        const updatedTask = await Task.findByIdAndUpdate(id, updates, {
            new: true,
            runValidators: true,
        });
        if (!updatedTask) {
            res.status(404).json({ message: "Task not found" });
            logger.warn(`Task not found: ${id}`);
            return;
        }
        res.status(200).json(updatedTask);
        logger.info(`Updated task: ${id}`);
    } catch (error) {
        res.status(400).json({ message: error.message });
        logger.error(`Error updating task: ${id} - ${error.message}`);
    }
};

// Delete a task
const deleteTask = async (req, res) => {
    const { id } = req.params;

    try {
        const deletedTask = await Task.findByIdAndDelete(id);
        if (!deletedTask) {
            res.status(404).json({ message: "Task not found" });
            logger.warn(`Task not found: ${id}`);
            return;
        }
        res.status(200).json({ message: "Task deleted successfully" });
        logger.info(`Deleted task: ${id}`);
    } catch (error) {
        res.status(500).json({ message: error.message });
        logger.error(`Error deleting task: ${id} - ${error.message}`);
    }
};

module.exports = {
    getAllTasks,
    createTask,
    getTaskById,
    updateTask,
    deleteTask,
};

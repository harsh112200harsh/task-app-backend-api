const { z } = require("zod");

const taskSchema = z.object({
    title: z.string().min(1, { message: "Title is required" }).trim(),
    description: z
        .string()
        .min(1, { message: "Description is required" })
        .trim(),
    completed: z.boolean({ message: "Completed status is required" }),
    dueDate: z.string().datetime({ message: "Valid due date is required" }),
});

const validateTask = (req, res, next) => {
    try {
        req.body = taskSchema.parse(req.body);
        next();
    } catch (error) {
        res.status(400).json({ message: error.errors[0].message });
    }
};

module.exports = validateTask;

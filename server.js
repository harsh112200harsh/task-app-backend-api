require("dotenv").config();
const express = require("express");
const connectDB = require("./config/db");
const logger = require("./utils/logger");
const taskRouter = require("./routes/taskRouter");
const cors = require("cors");
const app = express();
const port = process.env.PORT;

// Function to add middlewares
const addMiddlewares = () => {
    // Middleware for cors
    app.use(cors());

    // Middleware for parsing JSON request body
    app.use(express.json());

    // Logging middleware
    app.use((req, res, next) => {
        logger.info(`${req.method} ${req.url}`);
        next();
    });
};

// Attach routes
const attachRoutes = () => {
    app.use("/api/tasks", taskRouter);
};

app.get("/healthcheck", (req, res) => {
    res.send("api is running fine");
});

// Connect to database and start server
const startServer = async () => {
    try {
        await connectDB();
        addMiddlewares();
        attachRoutes();

        app.listen(port, () => {
            logger.info(`Server is running on port ${port}`);
        });
    } catch (error) {
        logger.error(`Failed to start server: ${error.message}`);
        process.exit(1); // Exit process with failure
    }
};

startServer();
